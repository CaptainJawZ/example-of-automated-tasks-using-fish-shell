#!/usr/bin/env fish
set directories ~/Pictures/'To Organize'/ ~/Downloads/

function generate_random_number
    set min 0
    set max 9999999999
    set pad 10
    set -g random_number (string pad -w $pad --char=0 (random $min $max))
end

function test_name
    generate_random_number
    find $directories -type f -name "*$random_number*"
    set files (find ~/Pictures/ ~/Downloads -type f -name "*$random_number*")
    echo "The generated filename is:" $random_number
    while test -n "$files"
        echo "The following files have a duplicated filename:"
        echo $files
        echo "Conflicts found, generating a new filename"
        generate_random_number
    end
end

for file in (find $directories -maxdepth 1 -type f)
    if test (string match -i -r \
            'source|tenor|media|duckduckgo\.com|giphy|
            |image|^download|unknown|zoom|new_canvas|untitled' \
            (basename $file))
        test_name
        echo renaming
        echo (realpath $file)
        echo into
        echo (realpath (dirname $file)/$random_number)
        mv (dirname $file)/{(basename $file), $random_number}
    end
    if test (string match -i -r 'Screenshot_\d{8}' (basename $file))
        mv $file ~/Pictures/Screenshots/
    end
end

for dir_for_classifier in ~/Pictures/Photos/"Camera Roll" ~/Pictures/Screenshots
    set file_count (count (find $dir_for_classifier -maxdepth 1 -type f \
        -not -path $dir_for_classifier/'.stignore'))
    if test $file_count -gt 0
        echo "Running classifier"
        classifier -dt -df YYYY/MM -d $dir_for_classifier \
            -o $dir_for_classifier
        rm ~/.classifier-master.conf
    end
end

set dir_steam ~/.local/share/Steam/userdata/107446271/760/remote
set game_id 386360 960090 648800 262060 234140
set game_name Smite "Bloons Tower Defense 6" Raft "Darkest Dungeon" \
    "Mad Max"

if test (count $game_id) -eq (count $game_name)
    for i in (seq 1 (count $game_id))
        set dir_game $dir_steam/$game_id[$i]/screenshots
        set dir_dest ~/Pictures/Screenshots/Games/$game_name[$i]
        if test -e $dir_game
            if test (count (find $dir_game -maxdepth 1 -type f)) -gt 0
                mkdir -p $dir_dest
                echo Moving $game_name[$i] screenshots
                for file in (find $dir_game -maxdepth 1 -type f)
                    mv $file $dir_dest
                end
                echo Deleting $game_name[$i] thumbnails
                rm -rf $dir_game/thumbnails
            end
        end
    end
end

set cyberpunk_dir ~/Games/cyberpunk-2077/drive_c/users/jawz/Pictures/"Cyberpunk 2077"
if test -e $cyberpunk_dir
    for file in (find $cyberpunk_dir -type f)
        echo "Moving cyberpunk screenshots"
        mv $file ~/Pictures/Screenshots/Games/"Cyberpunk 2077/"
    end
end

set proton_dir ~/.steam/steam/compatibilitytools.d
if test -e $proton_dir
    for protonver in (find $proton_dir -mindepth 1 -maxdepth 1 -type d)
        set lutrisdir ~/.local/share/lutris/runners/wine/(basename $protonver)
        if test \( -e $lutrisdir \) -a \( -L $lutrisdir \)
            echo $lutrisdir
        else
            echo Symlink $lutrisdir "doesn't exist"
            echo Creating $lutrisdir...
            ln -s $protonver/files $lutrisdir
        end
    end
end
find ~/.local/share/lutris/runners/wine -xtype l -delete

for file in (find $directories -maxdepth 1 -type f)
    set correct_ext (string lower (file --mime-type $file | grep -oP '\w+$'))
    set cur_ext (string split -r -m1 . (basename $file))[2]
    set cur_name (string split -r -m1 . (basename $file))[1]
    if test (string match -i -r 'jpe|jpg|jpeg|png|gif' $correct_ext)
        if test "$cur_ext" != $correct_ext
            echo "The file" (basename $file) \
                "will be renamed so the propper extension is" $correct_ext
            set new_name $cur_name\.$correct_ext
            mv (dirname $file)/{(basename $file), $new_name}
        end
    end
end

if test -e ~/.pki
    rm -rf ~/.pki
end
moviezip
